package io.atlassian.aws
package dynamodb

import kadai.Invalid
import org.joda.time.DateTime
import org.junit.runner.RunWith
import scalaz.syntax.id._
import scalaz.{-\/, \/}
import scodec.bits.ByteVector
import spec.ScalaCheckSpec

@RunWith(classOf[org.specs2.runner.JUnitRunner])
class DecoderSpec extends ScalaCheckSpec {

  def is = s2"""
    Decoder should
      not fall over if it fails to decode long      $longDecodeHandlesExceptions
      not fall over if it fails to decode int       $intDecodeHandlesExceptions
      not fall over if it fails to decode DateTime  $dateTimeDecodeHandlesExceptions
      not fall over if it fails to decode String    $stringDecodeHandlesExceptions
      not fall over if it fails to decode options   $optionDecodeHandlesExceptions
      not fall over if it fails to decode binary    $byteBufferDecodeHandlesExceptions
      propagate failure from mapAttempt function    $mapAttemptPropagatesExceptions
      not discard errors when composing with or     $or
  """

  import Encoder._
  import Decoder._

  def longDecodeHandlesExceptions =
    (Encoder[String].encode("Foo") |> Decoder[Long].decode).toOr.toEither must beLeft

  def intDecodeHandlesExceptions =
    (Encoder[String].encode("Foo") |> Decoder[Int].decode).toOr.toEither must beLeft

  def dateTimeDecodeHandlesExceptions =
    (Encoder[String].encode("Foo") |> Decoder[DateTime].decode).toOr.toEither must beLeft

  def stringDecodeHandlesExceptions =
    (Encoder[Int].encode(100) |> Decoder[String].decode).toOr.toEither must beLeft

  def optionDecodeHandlesExceptions =
    (Encoder[Int].encode(100) |> Decoder[Option[String]].decode) === Attempt.ok(None)

  def byteBufferDecodeHandlesExceptions =
    (Encoder[Int].encode(100) |> Decoder[NonEmptyBytes].decode).toOr.toEither must beLeft

  def mapAttemptPropagatesExceptions =
    (Encoder[NonEmptyBytes].encode(ByteVector.fromByte(1) match {
      case NonEmptyBytes(b) => b
    }) |> Decoder[TwoLongs].decode).toOr.toEither must beLeft

  def or = {
    val arbDecoder: Decoder[Int] =
      Decoder[Int].or(Decoder[Int]).or(Decoder[Int])

    (Encoder[String].encode("Foo") |> arbDecoder.decode).toOr must beLike[Invalid \/ Int] {
      case -\/(Invalid.Composite(nel)) => nel.size aka "number of decode errors" must_=== (3)
    }
  }
}
